/*
    lua2go
    Copyright (C) 2019  Zaoqi <zaomir@outlook.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import * as lua from "luaparse";
import * as assert from "assert";
export type LuaAST=lua.Node

function object_shadow_copy(x:{[key:string]:any}):{[key:string]:any}{
    return Object.assign({},x)
}

function pre(x:string):LuaAST{
    return lua.parse(x,{comments:false,scope:false,locations:false})
}
export {pre}

type Go=string

const lua_type="interface{}"
const lua_num_type="float64"
const lua_nil="_lua_nil"
const lua_VarargLiteral_id="_lua_tmp_vararg"
const lua_true="_lua_true"
const lua_false="_lua_false"
function lua_lookup(map:Go,key:Go):Go{
    return `_lua_lookup(${map},${key})`
}
function lua_set(map:Go,key:Go,val:Go):Go{
    return `_lua_set(${map},${key},${val})`
}
function lua_table(map:Array<[Go,Go]>):Go{
    return `_lua_table([${map.map(x=>`[${x[0]}, ${x[1]}]`).reduce((x,y)=>x+", "+y)}])`
}
function lua_str(x:string):Go{
    return `_lua_str(${JSON.stringify(x)})`
}
function lua_num(x:number):Go{
    return `_lua_num(${x.toString()})`
}
function lua_call(x:Go,args:Array<Go>):Go{
    if(args.length===0){return `_lua_call(${x}, vec![])`}
    return `_lua_call(${x}, [${args.reduce((x,y)=>x+", "+y)}])`
}
function lua_vec(vec:Go):Go{
    return `_lua_vec(${vec})`
}
function lua_lambda(arg_id:Go,body:Go):Go{
    return `_lua_lambda(func(${arg_id} [](${lua_type})) ${lua_type} {\n${body}\nreturn ${lua_nil}\n})`
}
function lua_len(x:Go):Go{
    return `_lua_len(${x})`
}
function lua_not(x:Go):Go{
    return `(!${lua_as_bool(x)})`
}
function lua_as_bool(x:Go):Go{
    return `_lua_as_bool(${x})`
}
function lua_as_num(x:Go):Go{
    return `_lua_as_num(${x})`
}
function lua_as_int(x:Go):Go{
    return `_lua_as_int(${x})`
}
function lua_as_str(x:Go):Go{
    return `_lua_as_str(${x})`
}
function lua_op(op:"+" | "-" | "*" | "/" | "%" | "^" | "==" | "~=" | "<=" | ">=" | "<" | ">" | ".." | "or" | "and",x:Go,y:Go){
    const numops: any={
        "+":"+",
        "-":"-",
        "*":"*",
        "/":"/",
        "==": "==",
        "~=": "!=",
        "<=": "<=",
        ">=": ">=",
        "<": "<",
        ">": ">",
    }
    if(op in numops){
        const o: any = numops[op as any]
        return `(${lua_as_num(x)} ${o} ${lua_as_num(y)})`
    }
    if(op == "%") {
        return `_lua_num(${lua_as_int(x)} % ${lua_as_int(y)})`
    }
    if(op === "^") {
        return `Math.Pow(${lua_as_num(x)}, ${lua_as_num(y)})`
    }
    if(op === "..") {
        return `(${lua_as_str(x)} + ${lua_as_str(y)})`
    }
    if(op === "and") {
        throw 'WIP'
    }
    if(op === "or") {
        throw 'WIP'
    }
    throw ''
}
const table:{[key:string]:(x:any)=>Go}={
    Chunk: (x:lua.Chunk)=>x.body.length===0?"":x.body.map(do_table).reduce((x,y)=>x+y),
    LabelStatement:(x:lua.LabelStatement)=>`${x.label}:\n`,
    BreakStatement:(x:lua.BreakStatement)=>'break;\n',
    GotoStatement:(x:lua.GotoStatement)=>`goto ${x.label}\n`,
    ReturnStatement:(x:lua.ReturnStatement)=>{
        if(x.arguments.length===0){
            return `return ${lua_nil}\n`
        }
        assert(x.arguments.length===1,`multiple values returning not supported`)
        return `return ${do_table(x.arguments[0])}\n`
    },
    IfStatement:do_IfStatement,
    WhileStatement:(x:lua.WhileStatement)=>`for ${lua_as_bool(do_table(x.condition))} {\n${do_table_map(x.body)}}\n`,
    DoStatement:(x:lua.WhileStatement)=>`{\n${do_table_map(x.body)}}\n`,
    RepeatStatement:(x:lua.RepeatStatement)=>
        `for {\n${do_table_map(x.body)}if ${lua_as_bool(do_table(x.condition))} { break; }\n}\n`,
    LocalStatement:do_LocalStatement,
    AssignmentStatement:do_AssignmentStatement,
    CallStatement:(x:lua.CallStatement)=>`${do_table(x.expression)};\n`,
    FunctionDeclaration:do_FunctionDeclaration,
    ForNumericStatement:do_ForNumericStatement,
    ForGenericStatement:do_ForGenericStatement,
    Identifier: (x:lua.Identifier)=>`${x.name}`,
    StringLiteral:(x:lua.StringLiteral)=>lua_str(x.value),
    NumericLiteral:(x:lua.NumericLiteral)=>lua_num(x.value),
    BooleanLiteral:(x:lua.BooleanLiteral)=>x.value?lua_true:lua_false,
    NilLiteral:()=>lua_nil,
    TableConstructorExpression:do_TableConstructorExpression,
    UnaryExpression:(x:lua.UnaryExpression)=>{
        if(x.operator==='#'){
            return lua_len(do_table(x.argument))
        }else{
            const _not:'not'=x.operator
            return lua_not(do_table(x.argument))
        }
    },
    BinaryExpression:(x:lua.BinaryExpression)=>lua_op(x.operator,do_table(x.left),do_table(x.right)),
    LogicalExpression:(x:lua.LogicalExpression)=>lua_op(x.operator,do_table(x.left),do_table(x.right)),
    MemberExpression:(x:lua.MemberExpression)=>{
        assert(x.indexer==='.')
        return lua_lookup(do_table(x.base),lua_str(x.identifier.name))
    },
    IndexExpression:(x:lua.IndexExpression)=>lua_lookup(do_table(x.base),do_table(x.index)),
    CallExpression:(x:lua.CallExpression)=>lua_call(do_table(x.base),x.arguments.map(do_table)),
    TableCallExpression:(x:lua.TableCallExpression)=>lua_call(do_table(x.base),[do_table(x.arguments)]),
    StringCallExpression:(x:lua.StringCallExpression)=>lua_call(do_table(x.base),[do_table(x.argument)]),
}

function do_AssignmentStatement(x:lua.AssignmentStatement):Go{
    assert(x.init.length===1 && x.variables.length===1)
    const [vara,init]=[x.variables[0],x.init[0]]
    if(vara.type==='Identifier'){
        return `${vara.name} = ${do_table(init)}`
    }else if(vara.type==='MemberExpression'){
        assert(vara.indexer==='.')
        return lua_set(do_table(vara.base),lua_str(vara.identifier.name),do_table(init))
    }else{
        const _i:'IndexExpression'=vara.type
        return lua_set(do_table(vara.base),do_table(vara.index),do_table(init))
    }
}

function do_TableConstructorExpression(x:lua.TableConstructorExpression):Go{
    if(x.fields.length===1&&x.fields[0].type==='TableValue'&&x.fields[0].value.type==='VarargLiteral'){
        return `${lua_VarargLiteral_id}`
    }
    const map:Array<[Go,Go]>=[]
    let i=1
    const fs:Array<lua.TableKey | lua.TableKeyString | lua.TableValue>=x.fields
    for(const item of fs){
        assert(item.value.type!=='VarargLiteral','`{<other>, ...}` not supported')
        if(item.type==='TableKey'){
            map.push([do_table(item.key),do_table(item.value)])
        }else if(item.type==='TableKeyString'){
            map.push([lua_str(item.key.name),do_table(item.value)])
        }else{ // TableValue
            map.push([lua_num(i),do_table(item.value)]) // 此处可能和Lua实现不同。
            i++
        }
    }
    return lua_table(map)
}

function do_ForGenericStatement(x:lua.ForGenericStatement):Go{
    throw 'WIP: ForGenericStatement'
}

function do_ForNumericStatement(x:lua.ForNumericStatement):Go{
    throw 'WIP: ForNumericStatement'
}

function do_FunctionDeclaration(x:lua.FunctionDeclaration):Go{
    const id: lua.MemberExpression | lua.Identifier | null = x.identifier as any
    if(id===null){
        const arg_id=`_lua_arg_tmp`
        let result=""
        const args:Array<lua.Identifier | lua.VarargLiteral>=x.parameters
        for(let i=0;i<args.length;i++){
            const arg=args[i]
            if(arg.type==='VarargLiteral'){
                result+=`var ${lua_VarargLiteral_id} ${lua_type} = ${lua_vec(arg_id)};\n`
            }else{ // Identifier
                result+=`var ${arg.name} ${lua_type}\nif len(${arg_id}) == 0 {\n${arg.name} = ${lua_nil}\n} else {\n${arg.name} = ${arg_id}[0]\n${arg_id} = ${arg_id}[1:]\n}\n`
            }
        }
        result+=do_table_map(x.body)
        return lua_lambda(arg_id,result)
    }else{
        if(id.type==='MemberExpression'){
            if(id.indexer===':'){
                throw '`function o:m() end` not implemented'
            }else{
                let _i:'.'=id.indexer
            }
        }
        const func=object_shadow_copy(x)
        func.identifier=null
        return do_table({type: x.isLocal?"LocalStatement":"AssignmentStatement",
                         variables:[id],
                         init:[func]} as any)
    }
}

function do_LocalStatement(x:lua.LocalStatement):Go{
    if(x.init.length===0){
        return x.variables.map(x=>`var ${x.name} ${lua_type} = ${lua_nil}\n`).reduce((x,y)=>x+y)
    }else{
        assert(x.init.length===1 && x.variables.length===1, `multiple values assign not supported`)
        return `var ${x.variables[0].name} ${lua_type} = ${do_table(x.init[0])}\n`
    }
}

function do_IfStatement(x:lua.IfStatement):Go{
    let result:Go=""
    const cs:Array<lua.IfClause | lua.ElseifClause | lua.ElseClause>=x.clauses
    for(const c of cs){
        if(c.type==='IfClause'){
            result+=`if ${do_table(c.condition)} {\n`
            result+=do_table_map(c.body)
            result+=`}`
        }else if(c.type==='ElseifClause'){
            result+=` else if ${do_table(c.condition)} {\n`
            result+=do_table_map(c.body)
            result+=`}`
        }else if(c.type==='ElseClause'){
            result+=` else {\n`
            result+=do_table_map(c.body)
            result+=`}`
        }
    }
    result+=`\n`
    return result
}

function do_table_map<K extends string>(xs:Array<lua.Statement>):Go{
    return do_table({type:"Chunk",body:xs} as lua.Chunk)
}

function do_table<K extends string>(x:lua.Base<K>):Go{
    assert(x.type in table,`${x.type} not supported`)
    return table[x.type](x)
}
const inner_compile=do_table
export {inner_compile}
